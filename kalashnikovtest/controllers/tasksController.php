<?php

require_once 'models/Task.php';
require_once 'models/User.php';

class TasksController
{
    
    /*
     * Create new task
     */
    public function create()
    { 
      
        if(isset($_POST['btn_send']))
        {
            $modelTask = new Task();
            
            $name   = $_POST['name'];
            $email  = $_POST['email'];
            $text   = $_POST['text'];
            $image  = $_POST['image'];
            
            $msg = $modelTask->create($name,$email,$text,$image);
            
            if($msg['result']) 
            {
                header('Location: ' . '/');
            }
            else
            {
                $msg = $msg['text'];
            }
        }
       
        $content = 'views/tasks/create.php';
        require_once 'views/template.php';

    }
    
    /*
     * Update task
     */
    public function update()
    { 
        $id = $_REQUEST['id'];
        
        $modelTask = new Task();
        
        
        if(isset($_POST['btn_send']))
        {
         
            $text     = $_POST['text'];
            $is_done  = isset($_POST['is_done']) ? 1 : 0;
            
            $msg = $modelTask->update($id,$text,$is_done);
            
            if($msg['result']) 
            {
                header('Location: ' . '/');
            }
            else
            {
                $msg = $msg['text'];
            }
        }
        
        
        $task = $modelTask->get_by_id($id);

        $content = 'views/tasks/update.php';
        require_once 'views/template.php';

    }
    
    /*
     * Ajax task preview
     */
    public function preshow()
    { 
        $modelTask = new Task();
        
        $name   = $_REQUEST['name'];
        $email  = $_REQUEST['email'];
        $text   = $_REQUEST['text'];
        $image  = $_REQUEST['image'];
       
        $image = $modelTask->save_image('tmp/');
        $image = '/files/tmp/'.$image['content'];
        
        $content = include 'views/tasks/preview.php';

        echo json_encode($content);
        exit;

    }
}
