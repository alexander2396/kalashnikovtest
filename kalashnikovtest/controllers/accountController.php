<?php

require_once 'models/User.php';

class AccountController
{

    /*
     * Login
     */
    public function login()
    {
 
        if(isset($_POST['btn_send']))
        {
            $modelUser = new User();
            
            $login = $_POST['login'];
            $password = $_POST['password'];
          
            $user = $modelUser->login($login,$password);
            
            if(is_array($user))
            {
                $_SESSION['user'] = $user; 
                header('Location: ' . '/');
            }
            else
            {
                $msg = "<div class='alert alert-danger' role='alert'>Неверный логин или пароль</div>";
            }
        }
        
        $content = 'views/account/login.php';
        require_once 'views/template.php';
    }
    
    /*
     * Logout
     */
    public function logout()
    {
 
        unset($_SESSION['user']);
        header('Location: ' . '/');

    }
}
