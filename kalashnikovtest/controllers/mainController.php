<?php

require_once 'models/Task.php';

class MainController
{
    /*
     * Main page
     */
    public function index()
    {
        
        $modelTask = new Task();
        
        $sort = isset($_REQUEST['sort']) ? $_REQUEST['sort'] : 'id';
        if($sort == 'is_done') $sort .= ' DESC';
 
        $tasks = $modelTask->get_list($sort);
        
        $content = 'views/main/index.php';
        require_once 'views/template.php';
    }
}
