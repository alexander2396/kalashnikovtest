<?php


class Task 
{

    /*
     * New task
     */
    public function create($name,$email,$text,$image)
    {
        $mysqli = @new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
        
        mysql_real_escape_string($name);
        mysql_real_escape_string($email);
        
        $image = $this->save_image();
        
        if($image['result'] == FALSE)
        {
            return array('result' => FALSE, 'text' => $image['msg']);
        }
        else
        {
            $image = $image['content'];
        }
        
        
        $result = $mysqli->query ("INSERT INTO tasks (`name`, `email`, `text`, `image`) "
                . "VALUES ('$name','$email','$text','$image')");
        
        if(!$result) 
        {
            return array('result' => FALSE, 'text' => "<div class='alert alert-danger' role='alert'>".$mysqli->error."</div>");
	}
        
        return array('result' => TRUE, 'text' => "<div class='alert alert-success' role='alert'>Задача добавлена</div>");
    }
    
    /*
     * Update task
     */
    public function update($id,$text,$is_done)
    {
        $mysqli = @new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);    

        $result = $mysqli->query ("UPDATE tasks SET text = '".$text."', is_done = ".$is_done." WHERE id = ".$id);
        
        if(!$result) 
        {
            return array('result' => FALSE, 'text' => "<div class='alert alert-danger' role='alert'>".$mysqli->error."</div>");
	}
        
        return array('result' => TRUE, 'text' => "<div class='alert alert-success' role='alert'>Задача изменена</div>");
    }
    
    /*
     * Tasks list
     */
    public function get_list($sort)
    {
        $mysqli = @new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
       
        $result = $mysqli->query ("SELECT * FROM tasks ORDER BY ".$sort);
      
        for($i=0; $result->num_rows > $i;$i++) {	
		$rowset[] = $result->fetch_array(MYSQL_ASSOC);
	}
      
        return $rowset;
    }
    
    /*
     * Select task
     */
    public function get_by_id($id)
    {
        $mysqli = @new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
        
        $result = $mysqli->query ("SELECT * FROM tasks WHERE id =".$id);
	$result = $result->fetch_array(); 
	   
        return $result;
    }
    
    /*
     * Save image
     */
    public function save_image($path = false) {
  
        $img_types = array( "jpeg"  =>"image/jpeg",
			    "pjpeg" =>"image/pjpeg",
			    "png"   =>"image/png",
                            "x-png" =>"image/x-png",
                            "gif"   =>"image/gif",
                            );
        
        $type_img = array_search($_FILES['image']['type'],$img_types);
        
        if(!$type_img) 
        {
            return array('result' => FALSE, 'text' => "Wrong type img");
	}
        
        if(!move_uploaded_file($_FILES['image']['tmp_name'],FILES.$path.$_FILES['image']['name']))
        {
            return array('result' => FALSE, 'text' => "Error copy image");
        }
        
        if(!$this->resize_img($_FILES['image']['name'],$type_img))
        {
            return array('result' => FALSE, 'text' => "Eror to resize image");
        }
        
        return array('result' => TRUE, 'content' => $_FILES['image']['name']);
    }
    
    /*
     * Resize image
     */
    public function resize_img($file_name,$type,$path = false) {

	switch($type){
                case 'jpeg': $img_id = imagecreatefromjpeg(FILES.$path.$file_name); break; 
                case 'pjpeg': $img_id = imagecreatefromjpeg(FILES.$path.$file_name); break;
                case 'png': $img_id = imagecreatefrompng(FILES.$path.$file_name); break; 
                case 'x-png': $img_id = imagecreatefrompng(FILES.$path.$file_name); break; 
                case 'gif': $img_id = imagecreatefromgif(FILES.$path.$file_name); break;
	}
	
	$img_width = imageSX($img_id);
	$img_height = imageSY($img_id);	
        
        if($img_width < 320 && $img_height < 240)
            return TRUE;
        
	$img_new_width =  IMG_WIDTH;
	$img_new_height = IMG_HEIGHT;
	
	$img_dest_id = imagecreatetruecolor($img_new_width,$img_new_height);
	
	$result = imagecopyresampled($img_dest_id,
								 $img_id,
								 0,0,0,0,
								 $img_new_width,
								 $img_new_height,
								 $img_width,
								 $img_height);
	 switch($type){
                case 'jpeg': $img = imagejpeg($img_dest_id,FILES.$path.$file_name,100); break;
                case 'pjpeg': $img = imagejpeg($img_dest_id,FILES.$path.$file_name,100); break;
                case 'png': $img = imagepng($img_dest_id,FILES.$path.$file_name); break;
                case 'x-png': $img = imagepng($img_dest_id,FILES.$path.$file_name); break;
                case 'gif': $img = imagegif($img_dest_id,FILES.$path.$file_name); break;
	}
							 
	 imagedestroy($img_id);
	 imagedestroy($img_dest_id);
	 
	 if($img) {
	 	return TRUE;
	 }
	 else {
	 	return FALSE;
	 }
    }
}
