
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Test Work</title>
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="/assets/css/style.css" rel="stylesheet">
  </head>
  <body>
      <header class="container">
          <div class="header-inner">
                <?php if(isset($_SESSION['user'])):?>
                    <h3 style="float: left;margin-top: 0;">Добро пожаловать <?=$_SESSION['user']['login'];?></h3>
                    <a class="btn btn-default pull-right" href="/account/logout">Выйти</a>
                    <a class="btn btn-primary pull-right" href="/tasks/create" style="margin-right:20px">Новая задача</a>
                <?php else:?> 
                    <a class="btn btn-default pull-right" href="/account/login">Вход</a>
                    <a class="btn btn-primary pull-right" href="/tasks/create" style="margin-right:20px">Новая задача</a>
                <?php endif;?>  
          </div>
      </header>
      <div class="container">
            <?=$msg;?>
            <?php include $content;?>
      </div>
   
      <div id="myModal" class="modal fade">
        <div class="modal-dialog" style="width: 1200px;">
          <div class="modal-content">

            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
              <h4 class="modal-title">Предпросмотр задачи</h4>
            </div>

            <div class="modal-body">

            </div>
          </div>
        </div>
      </div>
      
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="/assets/js/bootstrap.min.js"></script>
    <script src="/assets/js/app.js"></script>
  </body>
</html>