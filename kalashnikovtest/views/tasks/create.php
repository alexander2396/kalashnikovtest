
    <form id="task-form" enctype="multipart/form-data" class="form-signin" method="POST">
        <h2 class="form-signin-heading">Новая задача</h2>
        
        <div class="form-group">
            <label for="name">Имя</label>
            <input name="name" type="text" class="form-control" id="name" placeholder="Введите имя" required="required">
        </div>
        
        <div class="form-group">
            <label for="email">Адрес email</label>
            <input name="email" type="email" class="form-control" id="email" placeholder="Введите email" required="required">
        </div>
        
        <div class="form-group">
            <label for="text">Текст задачи</label>
            <textarea name="text" class="form-control" id="text" rows="3" required="required"></textarea>
        </div>
        
        <div class="form-group">
            <label for="image">Картинка</label>
            <input name="image" type="file" class="form-control-file" id="image" required="required">
        </div>
        
        <button name="btn_send" class="btn btn-lg btn-primary btn-block" type="submit">Создать</button>
        
        <a class="btn btn-default" style="margin-top: 10px;" onclick="pre_show();">Предпросмотр</a>
    </form>
