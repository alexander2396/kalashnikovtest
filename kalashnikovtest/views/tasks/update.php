
    <form id="task-form" enctype="multipart/form-data" class="form-signin" method="POST">
        <h2 class="form-signin-heading">Редактирование задачи</h2>

        
        <div class="form-group">
            <label for="text">Текст задачи</label>
            <textarea name="text" class="form-control" id="text" rows="3"><?=$task['text'];?></textarea>
        </div>
        
        <div class="form-group">
            <div class="checkbox">
                <label><input name="is_done" type="checkbox" <?php if($task['is_done'] == 1) echo 'checked'; ?>>Выполнена</label>
            </div>
        </div>
        
        <button name="btn_send" class="btn btn-lg btn-primary btn-block" type="submit">Сохранить</button>

    </form>
