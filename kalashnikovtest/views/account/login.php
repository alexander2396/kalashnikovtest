
    <form class="form-signin" method="POST">
        
        <h2 class="form-signin-heading">Please sign in</h2>
        
        <div class="form-group">
            <label for="inputLogin" class="sr-only">Login</label>
            <input name="login" type="text" id="inputLogin" class="form-control" placeholder="Login" required autofocus>
        </div>
        
        <div class="form-group">
            <label for="inputPassword" class="sr-only">Password</label>
            <input name="password" type="password" id="inputPassword" class="form-control" placeholder="Password" required>
        </div>
        
        <button name="btn_send" class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
    </form>
