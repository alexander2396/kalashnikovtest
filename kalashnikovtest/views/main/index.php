<div class="row tasks">
    <div class="dropdown">
            <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
              Сортировать
              <span class="caret"></span>
            </button>
            <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
              <li><a href="?sort=name">Имя</a></li>
              <li><a href="?sort=email">Email</a></li>
              <li><a href="?sort=is_done">Статус</a></li>
            </ul>
          </div>
    <?php foreach($tasks as $task): ?>
      <div class="col-md-12">
          <div class="thumbnail" <?php if($task['is_done'] == 1): ?>style="background: #dff0d8;"<?php endif;?>>
          <div style="float: left;width: 320px;"><img src="/files/<?=$task['image'];?>" alt="..."> </div> 
          <div style="float: left; width: 800px;" class="caption">
            <h3><?=$task['name'];?></h3>
            <a href="mailto:<?=$task['email'];?>"><?=$task['email'];?></a>
            <p><?=$task['text'];?></p>
            <?php if(isset($_SESSION['user']) && $_SESSION['user']['role'] == 1): ?>
            <p>
                <a href="/tasks/update/?id=<?=$task['id'];?>" class="btn btn-primary" role="button">Редактировать</a> 
            </p>
            <?php endif;?>  
          </div>
          <div class="clearfix"></div>
        </div>
      </div>
    <?php endforeach; ?>
</div>
