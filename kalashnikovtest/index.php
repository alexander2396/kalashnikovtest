<?php

header("Content-Type:text/html;charset=UTF-8");

session_start();

require_once "config.php";
require_once 'routes.php';

$controller = isset($result['controller']) ? $result['controller'] : "main"; 

$action = isset($result['action']) ? $result['action'] : "index"; 

require_once 'controllers/'.$controller."Controller.php";

$controller_name = $controller.'Controller';

$controller = new $controller_name();
$controller->$action();

?>