function pre_show()
{
  
    var name = $('#name').val();
    var email = $('#email').val();
    var text = $('#text').val();
    var image = $('#image').val();
    var files = $('#image').files;
    
    var data = new FormData($('#task-form').get(0));
    

    
    $.ajax({
            url: '/tasks/preshow',
            type: 'POST',
            data : data,
            processData: false, 
            contentType: false, 
            success: function(json)
            {
                $('.modal-body').html(json.slice(0, -1));
                $("#myModal").modal('show'); 
            }
        }); 
}