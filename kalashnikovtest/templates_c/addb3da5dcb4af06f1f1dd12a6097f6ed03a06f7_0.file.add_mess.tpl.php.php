<?php
/* Smarty version 3.1.29, created on 2016-07-27 13:20:24
  from "C:\Apache24\htdocs\messbox\template\default\add_mess.tpl.php" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_57988ae8792f69_68276888',
  'file_dependency' => 
  array (
    'addb3da5dcb4af06f1f1dd12a6097f6ed03a06f7' => 
    array (
      0 => 'C:\\Apache24\\htdocs\\messbox\\template\\default\\add_mess.tpl.php',
      1 => 1469614822,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_57988ae8792f69_68276888 ($_smarty_tpl) {
?>
<h2>Новое объявление</h2>
<form method='post' enctype="multipart/form-data">
		Тема:<br>
			<input type='text' name='title' value="<?php echo $_SESSION['p']['title'];?>
">
		<br>
		Текст:<br>
			<textarea name="text"><?php echo $_SESSION['p']['text'];?>
</textarea>
		<br>
		Категории:<br />
		<select name="id_categories">
			<?php if ($_smarty_tpl->tpl_vars['categories']->value) {?>
				<?php
$_from = $_smarty_tpl->tpl_vars['categories']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_item_0_saved_item = isset($_smarty_tpl->tpl_vars['item']) ? $_smarty_tpl->tpl_vars['item'] : false;
$__foreach_item_0_saved_key = isset($_smarty_tpl->tpl_vars['key']) ? $_smarty_tpl->tpl_vars['key'] : false;
$_smarty_tpl->tpl_vars['item'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['key'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['item']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
$__foreach_item_0_saved_local_item = $_smarty_tpl->tpl_vars['item'];
?>
					<optgroup label="<?php echo $_smarty_tpl->tpl_vars['item']->value[0];?>
">
						<?php
$_from = $_smarty_tpl->tpl_vars['item']->value['next'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_v_1_saved_item = isset($_smarty_tpl->tpl_vars['v']) ? $_smarty_tpl->tpl_vars['v'] : false;
$__foreach_v_1_saved_key = isset($_smarty_tpl->tpl_vars['k']) ? $_smarty_tpl->tpl_vars['k'] : false;
$_smarty_tpl->tpl_vars['v'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['k'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['v']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['v']->value) {
$_smarty_tpl->tpl_vars['v']->_loop = true;
$__foreach_v_1_saved_local_item = $_smarty_tpl->tpl_vars['v'];
?>
							<option value="<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['v']->value;?>
</option>
						<?php
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_1_saved_local_item;
}
if ($__foreach_v_1_saved_item) {
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_1_saved_item;
}
if ($__foreach_v_1_saved_key) {
$_smarty_tpl->tpl_vars['k'] = $__foreach_v_1_saved_key;
}
?>
					</optgroup>
				<?php
$_smarty_tpl->tpl_vars['item'] = $__foreach_item_0_saved_local_item;
}
if ($__foreach_item_0_saved_item) {
$_smarty_tpl->tpl_vars['item'] = $__foreach_item_0_saved_item;
}
if ($__foreach_item_0_saved_key) {
$_smarty_tpl->tpl_vars['key'] = $__foreach_item_0_saved_key;
}
?>
			<?php }?>			
		</select>
		<br />
		
		Выберите тип объявления:<br />
		<?php if ($_smarty_tpl->tpl_vars['razd']->value) {?>
			<?php
$_from = $_smarty_tpl->tpl_vars['razd']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_item_2_saved_item = isset($_smarty_tpl->tpl_vars['item']) ? $_smarty_tpl->tpl_vars['item'] : false;
$_smarty_tpl->tpl_vars['item'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['item']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
$__foreach_item_2_saved_local_item = $_smarty_tpl->tpl_vars['item'];
?>
				<input type="radio" name="id_razd" value="<?php echo $_smarty_tpl->tpl_vars['item']->value['id'];?>
">
				<?php echo $_smarty_tpl->tpl_vars['item']->value['name'];?>

			<?php
$_smarty_tpl->tpl_vars['item'] = $__foreach_item_2_saved_local_item;
}
if ($__foreach_item_2_saved_item) {
$_smarty_tpl->tpl_vars['item'] = $__foreach_item_2_saved_item;
}
?>
		<?php }?>
			
		<br />
		Город:<br>
			<input type="text" name="town" value="<?php echo $_SESSION['p']['town'];?>
">
		<br>
		Основное изображение:<br>
			<input type="hidden" name="MAX_FILE_SIZE" value="">
			<input type="file" name="img"><br />
		Дополнительное изображение:<br>
			<input type="file" name="mini[]"><br />
			<input type="file" name="mini[]">
		<br /><br />
		
		Период актуальности объявления:<br />
		<select name="time">
			<option value="10">10 дней</option>
			<option value="15">15 дней</option>
			<option value="20">20 дней</option>
			<option value="30">30 дней</option>
		</select>
		<br />
		
		Цена:<br>
			<input type="text" name="price" value="<?php echo $_SESSION['p']['price'];?>
">
		<br>
		<!--
		Введите строку:<br>
			<img src="capcha.php"><br /><br /><input type="text" name="capcha">
		<br>
		-->
		<input type="submit" name="reg" value="Добавить">
		
	</form>
	<?php }
}
