<?php
/* Smarty version 3.1.29, created on 2016-07-27 13:49:55
  from "C:\Apache24\htdocs\messbox\template\default\categories.tpl.php" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_579891d39f95c0_61852190',
  'file_dependency' => 
  array (
    '913d18d012b41f9e77802436109e072f56098488' => 
    array (
      0 => 'C:\\Apache24\\htdocs\\messbox\\template\\default\\categories.tpl.php',
      1 => 1469616593,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_579891d39f95c0_61852190 ($_smarty_tpl) {
?>

<?php echo '<?php ';?>if (isset($_SESSION['msg'])) : <?php echo '?>';?>
	<?php echo '<?=';?>$_SESSION['msg'];<?php echo '?>';?>
	<?php echo '<?php ';?>endif;<?php echo '?>';?>
	<?php echo '<?php ';?>unset($_SESSION['msg']);<?php echo '?>';?>
<h1>Категория - <?php echo $_smarty_tpl->tpl_vars['name_cat']->value;?>
</h1>
<?php if ($_smarty_tpl->tpl_vars['name_razd']->value && $_smarty_tpl->tpl_vars['name_razd']->value != NULL) {?>
	<strong>Раздел: <?php echo $_smarty_tpl->tpl_vars['name_razd']->value;?>
</strong>
<?php }
if ($_smarty_tpl->tpl_vars['text']->value) {?>
	<?php
$_from = $_smarty_tpl->tpl_vars['text']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_item_0_saved_item = isset($_smarty_tpl->tpl_vars['item']) ? $_smarty_tpl->tpl_vars['item'] : false;
$_smarty_tpl->tpl_vars['item'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['item']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
$__foreach_item_0_saved_local_item = $_smarty_tpl->tpl_vars['item'];
?>
		<div id="mess">
			<h2 class = "title_p_mess"><a href="?action=view_mess&id=<?php echo $_smarty_tpl->tpl_vars['item']->value['id'];?>
"><?php echo $_smarty_tpl->tpl_vars['item']->value['title'];?>
</a></h2>
			<p class="p_mess_cat">
				<strong>Категория:</strong>
				 <?php echo $_smarty_tpl->tpl_vars['item']->value['cat'];?>
 | 
				<strong>Тип обьявления:</strong>
				  <?php echo $_smarty_tpl->tpl_vars['item']->value['razd'];?>
 | 
				<strong>Город:</strong>
				 <?php echo $_smarty_tpl->tpl_vars['item']->value['town'];?>

			</p>
			<p class="p_mess_cat">
				<strong>Дата добавления обьявления:</strong>
				 <?php echo date("d.m.Y",$_smarty_tpl->tpl_vars['item']->value['date']);?>
 | 
				<strong>Ценa:</strong> 
				 <?php echo $_smarty_tpl->tpl_vars['item']->value['price'];?>
 |
				<strong>Автор: </strong>
				<a href="<?php echo $_smarty_tpl->tpl_vars['item']->value['email'];?>
"><?php echo $_smarty_tpl->tpl_vars['item']->value['uname'];?>
</a>
			</p>
			<p class="mess_content"><img class="mini_mess" src="<?php echo @constant('SITE_NAME');
echo @constant('MINI');
echo $_smarty_tpl->tpl_vars['item']->value['img'];?>
">
				<?php echo nl2br($_smarty_tpl->tpl_vars['item']->value['text']);?>

			</p>
		
		</div>
		<hr>
	<?php
$_smarty_tpl->tpl_vars['item'] = $__foreach_item_0_saved_local_item;
}
if ($__foreach_item_0_saved_item) {
$_smarty_tpl->tpl_vars['item'] = $__foreach_item_0_saved_item;
}
?>
	<?php if ($_smarty_tpl->tpl_vars['navigation']->value) {?>
		<ul class="pager">
			<?php if (isset($_smarty_tpl->tpl_vars['navigation']->value['first'])) {?>
				<li class="first">
					<a href="?action=main&page=1<?php echo $_smarty_tpl->tpl_vars['id_r']->value;?>
">Первая</a>
				</li>
			<?php }?>
			<?php if (isset($_smarty_tpl->tpl_vars['navigation']->value['last_page'])) {?>
				<li>
					<a href="?action=main&page=<?php echo $_smarty_tpl->tpl_vars['navigation']->value['last_page'];
echo $_smarty_tpl->tpl_vars['id_r']->value;?>
">&lt;</a>
				</li>
			<?php }?>
			<?php if (isset($_smarty_tpl->tpl_vars['navigation']->value['previous'])) {?>
				<?php
$_from = $_smarty_tpl->tpl_vars['navigation']->value['previous'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_val_1_saved_item = isset($_smarty_tpl->tpl_vars['val']) ? $_smarty_tpl->tpl_vars['val'] : false;
$_smarty_tpl->tpl_vars['val'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['val']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['val']->value) {
$_smarty_tpl->tpl_vars['val']->_loop = true;
$__foreach_val_1_saved_local_item = $_smarty_tpl->tpl_vars['val'];
?>
				<li>
					<a href="?action=main&page=<?php echo $_smarty_tpl->tpl_vars['val']->value;
echo $_smarty_tpl->tpl_vars['id_r']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['val']->value;?>
</a>
				</li>
				<?php
$_smarty_tpl->tpl_vars['val'] = $__foreach_val_1_saved_local_item;
}
if ($__foreach_val_1_saved_item) {
$_smarty_tpl->tpl_vars['val'] = $__foreach_val_1_saved_item;
}
?>
			<?php }?>
			<?php if (isset($_smarty_tpl->tpl_vars['navigation']->value['current'])) {?>
				<li>
					<span><?php echo $_smarty_tpl->tpl_vars['navigation']->value['current'];?>
</span>
				</li>
			<?php }?>
			<?php if (isset($_smarty_tpl->tpl_vars['navigation']->value['next'])) {?>
				<?php
$_from = $_smarty_tpl->tpl_vars['navigation']->value['next'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_v_2_saved_item = isset($_smarty_tpl->tpl_vars['v']) ? $_smarty_tpl->tpl_vars['v'] : false;
$_smarty_tpl->tpl_vars['v'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['v']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['v']->value) {
$_smarty_tpl->tpl_vars['v']->_loop = true;
$__foreach_v_2_saved_local_item = $_smarty_tpl->tpl_vars['v'];
?>
				<li>
					<a href="?action=main&page=<?php echo $_smarty_tpl->tpl_vars['v']->value;
echo $_smarty_tpl->tpl_vars['id_r']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['v']->value;?>
</a>
				</li>
				<?php
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_2_saved_local_item;
}
if ($__foreach_v_2_saved_item) {
$_smarty_tpl->tpl_vars['v'] = $__foreach_v_2_saved_item;
}
?>
			<?php }?>
			<?php if (isset($_smarty_tpl->tpl_vars['navigation']->value['next_pages'])) {?>
				<li>
					<a href="?action=main&page=<?php echo $_smarty_tpl->tpl_vars['navigation']->value['next_pages'];
echo $_smarty_tpl->tpl_vars['id_r']->value;?>
">&gt;</a>
				</li>
			<?php }?>
			<?php if (isset($_smarty_tpl->tpl_vars['navigation']->value['end'])) {?>
				<li class="last">
					<a href="?action=main&page=<?php echo $_smarty_tpl->tpl_vars['navigation']->value['end'];
echo $_smarty_tpl->tpl_vars['id_r']->value;?>
">Последняя</a>
				</li>
			<?php }?>
	<?php }
}?>
	<?php echo '<?php ';?>unset($_SESSION['msg']);<?php echo '?>';
}
}
