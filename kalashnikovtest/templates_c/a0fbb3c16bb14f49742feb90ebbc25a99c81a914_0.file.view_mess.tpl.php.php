<?php
/* Smarty version 3.1.29, created on 2016-07-27 15:05:07
  from "C:\Apache24\htdocs\messbox\template\default\view_mess.tpl.php" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5798a3738094f0_13163532',
  'file_dependency' => 
  array (
    'a0fbb3c16bb14f49742feb90ebbc25a99c81a914' => 
    array (
      0 => 'C:\\Apache24\\htdocs\\messbox\\template\\default\\view_mess.tpl.php',
      1 => 1469621105,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5798a3738094f0_13163532 ($_smarty_tpl) {
?>

<?php echo '<?php ';?>if (isset($_SESSION['msg'])) : <?php echo '?>';?>
	<?php echo '<?=';?>$_SESSION['msg'];<?php echo '?>';?>
	<?php echo '<?php ';?>endif;<?php echo '?>';?>
	<?php echo '<?php ';?>unset($_SESSION['msg']);<?php echo '?>';?>

<?php if (isset($_SESSION['msg'])) {?>
  <?php echo $_SESSION['msg'];?>

<?php }?>

<h1><?php echo $_smarty_tpl->tpl_vars['text']->value['title'];?>
</h1>

<?php if ($_smarty_tpl->tpl_vars['text']->value) {?>
	
		<div id="mess">
			<h2 class = "title_p_mess"><a href="?action=view_mess&id=<?php echo '<?=';?>$text['id']<?php echo '?>';?>"><?php echo '<?=';?>$text['title']<?php echo '?>';?></a></h2>
			<?php if ($_smarty_tpl->tpl_vars['text']->value['confirm'] == 0) {?>
				<p class="no_confirm"><strong>Еще не подтверждено модератором</strong></p>
			<?php }?>
			<?php if ($_smarty_tpl->tpl_vars['text']->value['is_actual'] == 0) {?>
				<p class="no_actual"><strong>Уже не актуально</strong></p>
			<?php }?>
			<p class="p_mess_cat">
				<strong>Категория:</strong>
				 <?php echo $_smarty_tpl->tpl_vars['text']->value['cat'];?>
 | 
				<strong>Тип обьявления:</strong>
				  <?php echo $_smarty_tpl->tpl_vars['text']->value['razd'];?>
 | 
				<strong>Город:</strong>
				 <?php echo $_smarty_tpl->tpl_vars['text']->value['town'];?>

			</p>
			<p class="p_mess_cat">
				<strong>Дата добавления обьявления:</strong>
				 <?php echo date("d.m.Y",$_smarty_tpl->tpl_vars['text']->value['date']);?>
 | 
				<strong>Ценa:</strong> 
				 <?php echo $_smarty_tpl->tpl_vars['text']->value['price'];?>
 |
				<strong>Автор: </strong>
				<a href="<?php echo $_smarty_tpl->tpl_vars['item']->value['email'];?>
"><?php echo $_smarty_tpl->tpl_vars['text']->value['uname'];?>
</a>
			</p>
			<p class="mess_content"><img class="mini_mess" src="<?php echo @constant('SITE_NAME');
echo @constant('MINI');
echo $_smarty_tpl->tpl_vars['text']->value['img'];?>
">
				<?php echo nl2br($_smarty_tpl->tpl_vars['text']->value['text']);?>

			</p>
		<?php if ($_smarty_tpl->tpl_vars['img_s']->value && is_array($_smarty_tpl->tpl_vars['img_s']->value)) {?>
		<?php
$_from = $_smarty_tpl->tpl_vars['img_s']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_item_0_saved_item = isset($_smarty_tpl->tpl_vars['item']) ? $_smarty_tpl->tpl_vars['item'] : false;
$_smarty_tpl->tpl_vars['item'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['item']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
$__foreach_item_0_saved_local_item = $_smarty_tpl->tpl_vars['item'];
?>
			<img class="mini_mess" src="<?php echo @constant('SITE_NAME');
echo @constant('MINI');
echo $_smarty_tpl->tpl_vars['item']->value;?>
">
		<?php
$_smarty_tpl->tpl_vars['item'] = $__foreach_item_0_saved_local_item;
}
if ($__foreach_item_0_saved_item) {
$_smarty_tpl->tpl_vars['item'] = $__foreach_item_0_saved_item;
}
?>
		<?php }?>
		
		</div>

<?php }?>
	<?php echo '<?php ';?>unset($_SESSION['msg']);<?php echo '?>';
}
}
