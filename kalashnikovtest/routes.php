<?php

    $query_string = substr($_SERVER['REQUEST_URI'],1);

    //правила
    $rules = array(
        '/^(account)\/(login)$/' => array(
            'subrules' => array(
                'default' => array('controller' => 'account','action' => 'login'),
            )
        ),
        '/^(account)\/(logout)$/' => array(
            'subrules' => array(
                'default' => array('controller' => 'account','action' => 'logout'),
            )
        ),
        '/^(tasks)\/(create)$/' => array(
            'subrules' => array(
                'default' => array('controller' => 'tasks','action' => 'create'),
            )
        ),
        '/^(tasks)\/(update).*$/' => array(
            'subrules' => array(
                'default' => array('controller' => 'tasks','action' => 'update'),
            )
        ),
        '/^(tasks)\/(preshow).*$/' => array(
            'subrules' => array(
                'default' => array('controller' => 'tasks','action' => 'preshow'),
            )
        )
        );
        
    // начало разбора запроса
    $rule_found = false;

    // главная страница
    $result = get_home_page($query_string);
    if (isset($result)) $rule_found = true;      
    
        // поиск подходящего специфического правила
    if (!$rule_found)
    {
        $result = get_appropriate_rule($query_string, $rules);   
        if (isset($result)) $rule_found = true;
    }
    // правило на найдено
    if (!$rule_found)
    {
        return array('controller' => 'main','action' => 'index');
    }
 
/**
 * Ищет подходящее правило разбора в массиве правил для строки
 */
function get_appropriate_rule($query_string, $rules)
{ 
    foreach ($rules as $rule => $subrules)
    {        
        if (preg_match_all($rule, $query_string, $rule_matches))
        {         
            foreach ($subrules['subrules'] as $subrule => $result)
            {
                if ($subrule == 'default' || preg_match_all($subrule, $query_string, $matches))
                { 
                    if ($subrule == 'default') $matches = $rule_matches;
                    
                    foreach ($result as $param => $value)
                    {
                        if (!is_array($value))
                        {
                            if (preg_match('/\$[0-9]/', $value)) $result[$param] = $matches[intval($value[1])][0];
                        }
                        else
                        {
                            foreach ($value as $param1 => $value1)
                            {
                                if (preg_match('/\$[0-9]/', $value1)) $result[$param][$param1] = $matches[intval($value1[1])][0];
                            }
                        }
                    }
                    
                    return $result;
                }
            }
        }
    }
    
    return null;
}

/**
 * Определяет главную страницу
 */
function get_home_page($query_string)
{
    if (empty($query_string)) return array('action' => 'index');
    
    return null;
}

